(function(){

    function DefaultSites() {
        return [
            // {
            //     'id': 'uol-host',
            //     'linkLabel': 'UOL Host',
            //     'urlRedir': 'http://clicklogger.rm.uol.com.br/?prd=16&oper=11&grp=src:13;thm:barra_ecommerce;chn:%channel%;cpg:UOLHost_BarraEcommerce;creative:link_fixo_Host_BarraEcommerce&msr=Cliques%20de%20Origem:1&redir=http://www.uolhost.com.br/'
            // },
            {
                'id': 'pag-seguro',
                'linkLabel': 'PagSeguro',
                'urlRedir': 'http://clicklogger.rm.uol.com.br/?prd=32&oper=11&grp=src:13;thm:barra_ecommerce;chn:%channel%;creative:link_fixo_pagseguro&msr=Cliques%20de%20Origem:1&redir=http://pagseguro.uol.com.br'
            },
            {   
                'id': 'shopping-uol',
                'linkLabel': 'Shopping UOL',
                'urlRedir': 'http://clicklogger.rm.uol.com.br/?prd=97&oper=11&grp=src:13;thm:barra_ecommerce;chn:%channel%;creative:link_fixo_pagseguro&msr=Cliques%20de%20Origem:1&redir=http://shopping.uol.com.br'
            },
            {
                'id': 'boa-compra',
                'linkLabel': 'BoaCompra',
                'urlRedir': 'http://clicklogger.rm.uol.com.br/?prd=65&oper=11&grp=src:13;thm:barra_ecommerce;chn:%channel%;cpg:PagSeguro_Link&msr=Cliques%20de%20Origem:1&redir=http://www.boacompra.com'
            }
        ];
    };

    function TypeValidator () {
        
        var $public = this;
        $private = {};
        
        $public.isObject = function (entity) {
            return entity && entity.constructor === Object;
        };

        $public.isArray = function (value) {
            return value !== undefined && value.constructor === Array;
        };

        $public.isDefined = function (value) {
            return value !== undefined;
        };

        $public.isString = function (value) {
            return value !== undefined && (typeof value) === 'string';
        };

        return $public;
    }

    function ConfigValidator () {
        
        var $public = this;
        var $private = {};

        $private.validator = new TypeValidator();

        $public.isConfigValidator = function (config) {

            if (!$private.validator.isObject(config)) {
                return false;
            }

            if (!$private.validator.isString(config.width) || isNaN(config.width)) {
                return false;
            }

            if (config.activeId && !$private.validator.isString(config.activeId)) {
                return false;
            }

            if (!$private.validator.isString(config.selector)) {
                return false;
            }

            if (!$private.validator.isString(config.channel) || isNaN(config.channel)) {
                return false;
            }

            return true;
        }
    };

    function Styles(config, maxWidth) {

        var $public = this;
        var $private = {};
        $private.maxWidthMediaQuery = parseInt(config.width)+15;
        $private.styles =   '#tm-ecommerce-bar {display:block!important;width:100%;height:30px;background:#363636;margin:0;padding:0;box-sizing: border-box;font-family: "Arial",sans-serif;overflow: hidden;}';
        $private.styles +=  '#tm-ecommerce-bar img {border:none}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-content {max-width: '+config.width+'px;margin:0 auto;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-main {float:left;width:20%;height:30px;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-logo {position:relative;float:left;margin:0;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-logo img {margin-top:5px;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-home {position:relative;float:left;text-align:center;border-right:1px solid #404040;border-left:1px solid #404040;margin: 0 0 0 15px;height:30px;width:50px;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-home a {display: block;height: 30px;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-home img {margin-top: 6px;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu {font-size: 11px;font-weight: bold;float: left;width: 80%;text-align: right;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu ul {margin: 0;padding: 0;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu li {line-height: 30px;display: inline-block;height: 29px;list-style: none;text-transform: uppercase;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu li:hover {border-bottom: 2px solid #484848;background: #484848;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu li.active {border-bottom: 2px solid #e2e2e2;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu li.active:hover {border-bottom: 2px solid #e2e2e2;background-color: transparent;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu .last {border-right: 1px solid #404040;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu a, #tm-ecommerce-bar .tm-ecom-menu span {display: block;padding: 0 15px;text-decoration: none;color: #e2e2e2;border-left: 1px solid #404040;}';
        $private.styles +=  '#tm-ecommerce-bar .tm-ecom-menu span {cursor: default;}';
        $private.styles +=  '@media(max-width: '+$private.maxWidthMediaQuery+'px) {#tm-ecommerce-bar .tm-ecom-logo img {margin-left: 15px;}}';
        $private.styles +=  '@media(max-width: '+maxWidth+'px) {#tm-ecommerce-bar .tm-ecom-logo img {margin-left: 15px;}; #tm-ecommerce-bar .tm-ecom-menu {display:none;} #tm-ecommerce-bar .tm-ecom-main {height: 30px;margin: 0 auto;display: table; float: none; width:auto;} #tm-ecommerce-bar .tm-ecom-main .tm-ecom-home{display:none;}}';
        
        $public.getStyles = function() {
            return $private.styles;
        };
    };

    function BarCSS() {
        
        var $private = {};
        var $public = this;
        $private.MAX_WIDTH = 640;
        $private.styles = new Styles(window.UOLPD.TagManager.config, $private.MAX_WIDTH);
        
        $public.createStylesSheet = function() {
            var style = document.createElement('style');
            style.setAttribute('type', 'text/css');
            var text = $private.styles.getStyles();
            if (style.styleSheet) {
                style.styleSheet.cssText = text;
            } else {
                style.appendChild(document.createTextNode(text));
            }
            return style;
        };
    };

    function DomUtils (){
        
        var $private = {};
        var $public = this;
        
        $public.onReady = function(callback, querySelector, timeout){
            timeout = (timeout) ? timeout : 100;
            var time = setInterval(function(){
                if(document.body || document.querySelector(querySelector))  {
                    clearInterval(time);
                    callback();
                }
            }, timeout);
        };

        $public.append = function (selector, elements) {
            var parentDiv = document.querySelector(selector);
            if(!parentDiv) {
                return;
            }
            if (parentDiv.hasChildNodes()) {
                parentDiv.insertBefore(elements, parentDiv.childNodes[0]);
            } else {
                parentDiv.appendChild(elements);
            }
        };

        $public.applyAttributes = function(element, attributes){
            for(var key in attributes){
                element.setAttribute(key, attributes[key]);
            }
            return element;
        };

        $public.getBrowserWidth = function() {
            return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        };

        $public.isMSIE = function(version) {
            var pos, foundVersion;
            var appName = navigator.appName;
            var userAgent = navigator.userAgent;
            
            if (appName === "Microsoft Internet Explorer") {
                pos = userAgent.indexOf("MSIE");
                foundVersion = userAgent.substr(pos, userAgent.substr(pos).indexOf(';')).indexOf(version);
            } else if(appName === "Netscape") {
                pos = userAgent.indexOf("rv:11.0");
                foundVersion = userAgent.substr(pos, userAgent.substr(pos).indexOf(')')).indexOf(version);
            }
            return (foundVersion != -1);
        };

        $public.setOnResize = function(callback){
            var windowEvent = window.attachEvent || window.addEventListener;
            windowEvent('onresize',  function(e) {
                callback($public.getBrowserWidth());
            });
        };
    };

    function TagBuilder() {
        
        var $private = {};
        var $public = this;
        $private.barCss = new BarCSS();
        $private.domUtils = new DomUtils();

        $public.appendCss = function() {
            var styles = $private.barCss.createStylesSheet();
            var head = document.head || document.getElementsByTagName('head')[0];
            head.appendChild(styles);
        };

        $public.hasBodyBar = function(){
            if (document.getElementById('tm-ecommerce-bar')) {
                return true;
            }
            return false;
        };

        $public.createBodyBar = function() {
            var attributes = {'id':'tm-ecommerce-bar'};
            return $private.domUtils.applyAttributes(document.createElement('div'), attributes);
        };

        $public.createBarContent = function(){
            var attributes = {'class':'tm-ecom-content'};
            var div = document.createElement('div');
            div = $private.domUtils.applyAttributes(div, attributes);
            return div;
        };

        $public.getActiveSite = function (id, sites) {
            var currentSite;
            if(!id || !sites || sites.length === 0) {
                return;
            }
            for(var i = 0; i < sites.length; i++){
                if (id === sites[i].id) {
                    currentSite = sites[i];
                }
            }
            return currentSite;
        };

        $public.createNavRight = function(config, activeItem, sites){
            var ul, li, span, div, click, sites, currentChannel;
            
            div = $private.domUtils.applyAttributes(document.createElement('div'), {'class':'tm-ecom-menu'});
            ul = document.createElement('ul');

            var length = sites.length;

            for(var i = 0; i < length; i++){
                li = document.createElement('li');
                if (activeItem && activeItem.id && activeItem.id === sites[i].id) {
                    span = document.createElement('span');
                    var spanText = document.createTextNode(sites[i].linkLabel);
                    span.appendChild(spanText);
                    li.appendChild(span);
                    li.setAttribute('class', 'active');

                } else {
                    click = $private.createClickTag(config.channel, sites[i]);
                    var clickText = document.createTextNode(sites[i].linkLabel);
                    click.appendChild(clickText);
                    li.appendChild(click);
                }

                if (i === length-1) {
                    var attr = li.getAttribute('class');
                    if (attr) {
                        attr = attr.concat(' last');
                    } else {
                        attr = 'last';
                    }
                    li.setAttribute('class', attr);
                }

                ul.appendChild(li);
            }

            div.appendChild(ul);
            return div;
        };

        $public.createNavLeft = function(){
            var div, figureLogo, figureHome;
            var menuAttributes = {
                'class':'tm-ecom-main'
            }
            div = document.createElement('div');
            div = $private.domUtils.applyAttributes(div, menuAttributes);
            
            var attributesLogo = {
                'link' : {
                    'href' : 'http://www.uol.com.br/'
                },
                'figure' : {
                    'class':'tm-ecom-logo',
                    'title' : 'UOL - O melhor conteúdo'
                },
                'img' : {
                    'alt' :  'Ir para o UOL',
                    'src':  'http://imguol.com/tag-manager/ecommerce-bar/ecommerce-uol-logo.png'
                }
            };
            
            figureLogo = $private.createFigureElement(attributesLogo);

            var attributesHome = {
                'link' : {
                    'href' : 'http://tecnologia.uol.com.br/faca-do-uol-a-sua-pagina-inicial/'
                },
                'figure' : {
                    'class':'tm-ecom-home',
                    'title' : 'Faça do UOL sua home page'
                },
                'img' : {
                    'src': 'http://imguol.com/tag-manager/ecommerce-bar/ecommerce-home.png'
                }
            };
            
            figureHome = $private.createFigureElement(attributesHome);
            div.appendChild(figureLogo);
            div.appendChild(figureHome);
            return div;
        };

        $private.createFigureElement = function (attributes) {
            var figure, img, click, caption;
            figure = $private.domUtils.applyAttributes(document.createElement('figure'), attributes.figure);
            img = $private.domUtils.applyAttributes(document.createElement('img'), attributes.img);
            click = $private.domUtils.applyAttributes(document.createElement('a'), attributes.link);
            click.appendChild(img);
            figure.appendChild(click);
            return figure; 
        };

        $private.createClickTag = function(channel, item){
            var url, urlRoiMeter;
            urlRoiMeter = document.createElement('a');
            url = 'http://click.uol.com.br/?';
            url += 'rf=barraecommerce';
            url += '&u=';
            urlRoiMeter.href = url + item.urlRedir.replace('%channel%', channel);
            return urlRoiMeter;
        };

        $public.repositionElements = function(maxWidth,  width){
            var navRight = document.querySelector('.tm-ecom-menu');
            var main = document.querySelector('.tm-ecom-main');
            var home = document.querySelector('.tm-ecom-home');
            if (width <= maxWidth) {
                navRight.style.display = "none";
                home.style.display = "none";
                main.style.height = '30px';
                main.style.margin = '0 auto';
                main.style.display = 'table'; 
                main.style.float = 'none'; 
                main.style.width = 'auto';
            } else {
                main.style.height = '30px';
                main.style.float = 'left'; 
                main.style.width = '20%';
                navRight.style.display = "block";
                home.style.display = "block";
            }
        };
    };
    
    function EcommerceBar() {
        
        var $public = this;
        var $private = {};
        
        $private.domUtils = new DomUtils();
        $private.defaultSites = new DefaultSites();
        $private.configValidator = new ConfigValidator();
        $private.tagBuilder = new TagBuilder();
        $private.MAX_WIDTH = 640;
        
        $public.init = function(config){
            var logo, navLeft, navRight, bodyBar, barContent;
            
            if(!$private.configValidator.isConfigValidator(config) || $private.tagBuilder.hasBodyBar()) {
                return;
            }
            
            $private.domUtils.onReady(function(){
                $private.setConfig(config);
                $private.tagBuilder.appendCss();

                bodyBar = $private.tagBuilder.createBodyBar();
                barContent = $private.tagBuilder.createBarContent();
                navLeft = $private.tagBuilder.createNavLeft();
                barContent.appendChild(navLeft);
                
                $private.activeSite = $private.tagBuilder.getActiveSite(config.activeId, $private.defaultSites);
                navRight = $private.tagBuilder.createNavRight($private.getConfig(), $private.activeSite, $private.defaultSites);
                barContent.appendChild(navRight);
                bodyBar.appendChild(barContent);
                $private.domUtils.append(config.selector, bodyBar);
                
                if ($private.domUtils.isMSIE('8')) {
                    $private.tagBuilder.repositionElements($private.MAX_WIDTH, $private.domUtils.getBrowserWidth());
                    $private.domUtils.setOnResize(function (width) {
                        $private.tagBuilder.repositionElements($private.MAX_WIDTH, $private.domUtils.getBrowserWidth());
                    });
                }
            }, config.selector);

        };

        $private.getConfig = function () {
            return $private.config;
        };

        $private.setConfig = function (value) {
            $private.config = value;
        };

    };
    
    window.UOLPD = window.UOLPD || {};
    window.UOLPD.TagManager = window.UOLPD.TagManager || {};
    window.UOLPD.TagManager.config = {
        'activeId': 'shopping-uol',
        'selector':'body',
        'width': '1170',
        'channel':'115'
    };
    window.UOLPD.TagManager.EcommerceBar = new EcommerceBar();
    try{
        if (window.localStorage.toggleEcommerceBar === "true") {
            window.UOLPD.TagManager.EcommerceBar.init(window.UOLPD.TagManager.config);
        }
    }catch(e){}
})();

/*** DADOS DE PROD ***

## CONFIG SHOPPING UOL ##
    window.UOLPD.TagManager.config = {
        'activeId': 'shopping-uol',
        'selector':'body',
        'width': '1170',
        'channel':'37'
    };

## CONFIG SHOPPING BOL ##
    window.UOLPD.TagManager.config = {
        'selector':'body',
        'width': '1170',
        'channel':'71'
    };

## CONFIG PAGSEGURO ##
window.UOLPD.TagManager.config = {
    'activeId': 'pag-seguro',
    'selector':'???',
    'width': '???',
    'channel':'117'
};

## CONFIG BOACOMPRA ##
window.UOLPD.TagManager.config = {
    'activeId': 'boa-compra',
    'selector':'???',
    'width': '???',
    'channel':'497'
};

## CONFIG UOL HOST ##
window.UOLPD.TagManager.config = {
    'activeId': 'uol-host',
    'selector':'???',
    'width': '???',
    'channel':'118'
};


*/